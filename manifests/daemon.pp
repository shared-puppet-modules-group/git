class git::daemon {
  $manage_shorewall = false
) {

  require git

  case $::operatingsystem {
    debian: { include git::daemon::debian }
    centos: { include git::daemon::centos }
  }

  if $manage_shorewall {
    include shorewall::rules::gitdaemon
  }

}
